## usage: [d rho] = correlogram (u, v, Ju, Jv)
##
##
function [d rho] = correlogram (u, v, Ju, Jv)

   pkg load mapping
   
   if ~(isfield(u, "lons") & isfield(v, "lons")) ...
      ~(isfield(u, "lats") & isfield(v, "lats"))
      error("correlogram> no lons, lats") ;
   endif

   nu = sum(Ju) ; nv = sum(Jv) ;
   [Iu Iv] = common(u.id, v.id) ;
   
   u.lons = u.lons(Ju) ;
   u.lats = u.lats(Ju) ;
   v.lons = v.lons(Jv) ;
   v.lats = v.lats(Jv) ;

   d = cell2mat(arrayfun(@(i) cell2mat(arrayfun(@(j) distance(u.lats(i), u.lons(i), v.lats(j), v.lons(j)), 1:nv, "UniformOutput", false)), (1:nu)', "UniformOutput", false)) ;
   d = 2 * pi * d / 360 * 6350 ;

   u.x = u.x(Iu,Ju) ; v.x = v.x(Iv,Jv) ;
   rho = cell2mat(arrayfun(@(i) cell2mat(arrayfun(@(j) nancorrcoef(u.x(:,i), v.x(:,j)), 1:nv, "UniformOutput", false)), (1:nu)', "UniformOutput", false)) ;

endfunction
