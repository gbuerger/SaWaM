## usage: ncout (pfile, odir, vt, u, coord, ncdir)
##
## write netcdf output
function ncout (pfile, odir, vt, u, coord, ncdir)

   N = size(vt) ;

   s.ids = {"lower tercile" "central tercile" "upper tercile"} ;
   s.names = {"P terciles" "P terciles" "P terciles"} ;
   s.vars = {"tcl" "tcl" "tcl"} ;
   s.lons = repmat(coord(1), 1, 3) ;
   s.lats = repmat(coord(2), 1, 3) ;
   s.alts = repmat(coord(3), 1, 3) ;
   
   for y = 1:N(2)

      s.id = s.x = [] ;
      for m = 1:N(3)

	 ws.id = datevec(squeeze(vt(:,y,m,1)))(:,1:3) ;
	 
	 ws.x(:,1) = arrayfun(@(j) u{j,m}.cdf(y,1), 1:N(1)) ;
	 ws.x(:,3) = 1 - arrayfun(@(j) u{j,m}.cdf(y,2), 1:N(1)) ;
	 ws.x(:,[1 3]) = ws.x(:,[1 3]) ./ sum(ws.x(:,[1 3]), 2) ;
	 ws.x(:,2) = 1 - (ws.x(:,3) + ws.x(:,1)) ;

	 I = all(~isnan(ws.id), 2) ;
	 s.id = [s.id ; ws.id(I,:)] ;
	 s.x = [s.x ; ws.x(I,:)] ;

      endfor

      wdir = sprintf("%s/%4d/%s", odir, s.id(1,1), pfile) ;
      ncwrite_stat(wdir, s, [], "~/SaWaM/Nextcloud") ;
      
   endfor

endfunction
