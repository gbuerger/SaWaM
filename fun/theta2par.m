## usage: par = theta2par (theta, x)
##
## log-likelihood of ensemble
function par = theta2par (theta, x)

   global DIST
   
   x = squeeze(x) ;
   
   [a b c d] = deal(theta(1), theta(2), theta(3), theta(4)) ;

   xm = mean(x, 2) ;
   xs = std(x, 0, 2) ;

   switch DIST
      case "gam"
	 th = xs.^2 ./ xm ;
	 k = xm.^2 ./ xs.^2 ;
	 mu = a + b * th ;
	 sgm = c^2 + d^2 * k ;
      otherwise
	 mu = a + b * xm ;
	 sgm = c^2 + d^2 * xs ;
   endswitch

   par = struct("mu", mu, "sgm", sgm) ;
   
endfunction
