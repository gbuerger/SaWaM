## usage: [id, x] = read_XDS (ifile, nMET)
##
## read XDS input
function [id, x] = read_XDS (ifile, nMET)

   fmt = ["%d,%d,%d" repmat(",%f", 1, nMET)] ;

   x = cell(1, nMET) ;
   [y, m, d, x{:}] = textread(ifile, fmt, "headerlines", 1) ;
   id = reshape(repmat([y m d], 1, 25), [], 3, 25) ;
   id = permute(id, [1 3 2]) ;
   x = cell2mat(x) ;

endfunction
