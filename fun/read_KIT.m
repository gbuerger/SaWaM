## usage: [id, x] = read_KIT (ifile)
##
## read KIT input
function [id, x] = read_KIT (ifile)

   F = glob([ifile "/*.csv"])' ;

   fmt = "%04d-%02d-%02d\t%f" ;

   [Y, M, D, X] = parfun(@(f) textread(f, fmt), F, "UniformOutput", false) ;

   id = [Y{1} M{1}, D{1}] ;
   x = cell2mat(X) ;

endfunction
