## usage: f = pdfens (u, a, b, c, d, pm, ps)
##
## density of ensemble
function f = pdfens (u, a, b, c, d, pm, ps)

   mu = a + b * pm ;
   sg = c + d * ps ;
   
   f = normpdf((u - mu) ./ sg) ;

endfunction
