## usage: f = cdfens (u, a, b, c, d, pm, ps)
##
## density of ensemble
function f = cdfens (u, a, b, c, d, pm, ps)

   mu = a + b * pm ;
   sg = c + d * ps ;
   
   f = normcdf((u - mu) ./ sg) ;

endfunction
