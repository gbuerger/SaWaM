## usage: [f z] = ensks (theta)
##
## KS distance for theta
function [f z] = ensks (theta)

   global x y
   
   z = ensval(theta, x, y) ;
   [~, f] = kolmogorov_smirnov_test(z, "norm") ;

endfunction
