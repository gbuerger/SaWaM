## usage: y = tavg (x, Td, mode = "d")
##
## calculate daily or monthly mean
function y = tavg (x, Td, ID, mode = "d")

   if strcmp(mode, "d")
      
      for d = 1:365

	 if d < 61 + 29
	    I = Td == d ;
	 else
	    I = Td == d | Td == d + is_leap_year(ID(:,:,:,:,1)) ;
	 endif
	 y(d,1) = nanmean(x(I)) ;

      endfor

   else

      for m = 1:12

	 I = ID(:,:,:,:,2) == m ;
	 y(m,1) = nanmean(x(I)) ;

      endfor
      
   endif

endfunction
