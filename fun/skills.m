function res = EDS(a,s,r)
  z = 2*log(s) ;
  n = log(a) ;
  res = z./n -1 ;
endfunction

function res = PHI(a,s,r)
  z = a - s.*r ;
  n = sqrt(s.*(1-s).*r.*(1-r));
  if (n == 0)
    res = NaN ;
  else
    res = z./n;
  end
endfunction
function res = PHI0(B,H,F)
  z = (H-F).*(B-H) ;
  n = sqrt(B.*(B-H+(1-B).*F)) ;
  res = z./n ;
endfunction
function res = PHI1(B,H,F)
  s = F./(B-H+F) ;
  a = H.*s ;
  r = B.*s ;
  res = PHI(a,s,r) ;
endfunction


function res = PC(a,s,r)
  res = 1 + 2.*a - (s+r) ;
endfunction
function res = PC0(B,H,F)
  z = B - H - F.*B + 2.*H.*F ;
  n = B + F - H ;
  res = z./n ;
endfunction
function res = PC1(B,H,F)
  s = F./(B-H+F) ;
  a = H.*s ;
  r = B.*s ;
  res = PC(a,s,r) ;
endfunction

function res = HSS(a,s,r)
  z = a - s.*r ;
  n = (s+r)/2 - s.*r ;
  res = z./n ;
endfunction
function res = HSS0(B,H,F)
  z = 2.*(B-H).*(H-F) ;
  n = B.*(1+B-H-F) + F - H ;
  res = z./n ;
endfunction
function res = HSS1(B,H,F)
  s = F./(B-H+F) ;
  a = H.*s ;
  r = B.*s ;
  res = HSS(a,s,r) ;
endfunction

function res = PSS(a,s,r)
  z = a - s.*r ;
  n = s.*(1-s) ;
  res = z./n ;
endfunction
function res = PSS0(B,H,F)
  res = H - F ;
endfunction
function res = PSS1(B,H,F)
  s = F./(B-H+F) ;
  a = H.*s ;
  r = B.*s ;
  res = PSS(a,s,r) ;
endfunction

function res = CSS(a,s,r)
  z = a - s.*r ;
  n = r.*(1-r) ;
  if (n == 0)
    res = NaN ;
  else
    res = z./n ;
  end
endfunction
function res = CSS0(B,H,F)
  z = (H-F).*(B-H) ;
  n = B.*(B-H+F.*(1-B)) ;
  res = z./n ;
endfunction
function res = CSS1(B,H,F)
  s = F./(B-H+F) ;
  a = H.*s ;
  r = B.*s ;
  res = CSS(a,s,r) ;
endfunction

function res = CSI(a,s,r)
  res = a ./ (s+r-a) ;
endfunction
function res = CSI0(B,H,F)
  z = H ;
  n = 1 + B - H ;
  res = z./n ;
endfunction
function res = CSI1(B,H,F)
s = F./(B-H+F) ;
a = H.*s ;
r = B.*s ;
res = CSI(a,s,r) ;
endfunction

function res = GSS(a,s,r)
  z = a - s.*r ;
  n = a - s.*r + s + r ;
  res = z./n ;
endfunction
function res = GSS0(B,H,F)
  res = GSS1(B,H,F) ;
endfunction
function res = GSS1(B,H,F)
s = F./(B-H+F) ;
a = H.*s ;
r = B.*s ;
res = GSS(a,s,r) ;
endfunction

function res = Q(a,s,r)
  z = a - s.*r ;
  n = a + s.*r - 2.*a.*(s+r-a) ;
  if (n == 0)
    res = NaN ;
  else
    res = z./n ;
  end
endfunction
function res = Q0(B,H,F)
  z = H - F ;
  n = H + F - 2.*H.*F ;
  res = z./n ;
endfunction
function res = Q1(B,H,F)
  s = F./(B-H+F) ;
  a = H.*s ;
  r = B.*s ;
  res = Q(a,s,r) ;
endfunction
