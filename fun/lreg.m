## usage: out = lreg (x, y, yq, lmode)
##
## CV wrapper for lreg_
function out = lreg (x, y, yq, lmode)

   addpath("~/SaWaM/fun", "~/xds/fun", "~/xds/util")
   addpath ~/oct/nc/MLToolbox/MeteoLab/Validation
   pkg load statistics signal optim communications

   global QFOLD NFOLD

   xm = squeeze(mean(x, 4))(:) ;
   xs = squeeze(std(x, [], 4))(:) ;
   y = y(:) ;

   II = all(~isnan([xm xs y]), 2) ;
   x = [xm xs](II,:) ;
   y = lookup(yq, y(II,:)) ;

   N = size(x) ;

   ## full estimate
   I = 1 : N(1) ;
   out = lreg_ (x, y, I, I, yq, lmode) ;

   out.cdf = fillnan(out.cdf, II) ;

   ## cross validation
   I = arrayfun(@(i) randperm(N(1), round(QFOLD*N(1)))(:), 1:NFOLD, "UniformOutput", false) ;
   In = cellfun(@(i) setdiff(1:N(1), i)', I, "UniformOutput", false) ;

   wout = cellfun(@(i,in) lreg_ (x, y, i, in, yq, lmode), I, In, "UniformOutput", false) ;

   for k = {"theta" "rpss" "rsa"}
      out.(k{:}) = cellmean({[wout{:}].(k{:})}) ;
   endfor
   
endfunction

## usage: out = lreg_ (x, y, I, In, lmode)
##
## logistic regression, foll. Wilks (2006)
function out = lreg_ (x, y, I, In, lmode)

   global SCORES

   L = @(theta, u) (theta(:,1)' + u * theta(:,2:end)') ;
   Pfun = @(theta, u) logistic_cdf(L(theta, u)) ;

   if strcmp(lmode, "nlinfit")

      by = (y == unique(y)') ; ny = columns(by) ;
      theta0 = zeros(columns(x)+1, ny) ;
      opts = optimset("Display", "iter") ;
      function f = cost(theta, ny, by, x, Pfun)
	 theta = reshape(theta, [], ny) ;
	 f = sumsq((by - Pfun(theta, x))(:)) ;
      endfunction
      fun = @(theta) cost(theta, ny, by, x(I,:), Pfun) ;
      [theta, FVAL, INFO, OUTPUT, GRAD, HESS] = fminunc (fun, theta0(:)) ;
      theta = reshape(theta, size(theta0)) ;
      cdf = Pfun(theta, x) ;
      cdf = softmax(cdf) ;
      clear pdf ; pdf(:,1,:) = cdf ;

   else

      [theta, beta, dev, dl, d2l, cdf] = logistic_regression (y(I), x(I,:)) ;
      cdf = [logistic_cdf(theta' - x * beta) ones(rows(x),1)] ;
      clear pdf ; pdf(:,1,:) = diff([0*cdf(:,1) cdf], [], 2) ;

   endif

   addpath ~/oct/nc/MLToolbox/MeteoLab/common
   [rps, rpss] = validationRPS(y(In), pdf(In,:,:), 1:3) ;

   graph = false ;
   wrn = warning() ;
   warning("off", "Octave:divide-by-zero") ;
   skill(:,1) = validationBinaryProb([], y(In) == 0, cdf(In,1), SCORES, "points", 20, "graph", graph) ;
   skill(:,2) = validationBinaryProb([], y(In) == 2, 1 - cdf(In,2), SCORES, "points", 20, "graph", graph) ;
   warning(wrn) ;
   rmpath ~/oct/nc/MLToolbox/MeteoLab/common

   rsa = [skill.rsa] ;

   out = struct("theta", theta, "cdf", cdf, "rpss", rpss, "rsa", rsa) ;
   
endfunction
