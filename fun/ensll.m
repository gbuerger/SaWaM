## usage: f = ensll (theta)
##
## log-likelihood of ensemble
function f = ensll (theta)

   global y pm ps

   [a b c d] = deal(theta(1), theta(2), theta(3), theta(4)) ;
   
   mu = a + b * pm ;
   sg = c + d * ps ;
   
   z = (y - mu) ./ sg ;

   f = sum(1/2 * z.^2 - log(sg)) ;

endfunction
