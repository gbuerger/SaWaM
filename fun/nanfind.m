function i = nanfind(cond)
   if ~any(cond)
      i = NaN ;
   else
      i = find(cond) ;
   endif
endfunction
