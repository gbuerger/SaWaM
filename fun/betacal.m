## usage: res = betacal (x, yb, fun, opts, backend=@fminsearch)
##
## ensemble calibration
function varargout = betacal (x, yb, fun, opts, backend=@fminsearch)

   global OPTS
   
   x1 = [ones(rows(x), 1) x] ;
   pin = regress(yb, x1) ;

   varargout = cell(1, nargout) ;

   function res = cost (p)      
      I = all(~isnan([x yb]), 2) ;
      res = sumsq(fun(p, x(I,:)) - yb(I,:)) / sum(I) ;
   endfunction

   [varargout{:}] = feval(backend, @(p) cost(p), pin, OPTS) ;
   
endfunction
