## usage: xs = sfilter (x, SF)
##
## apply seamless filter
function xs = sfilter (x, SF)

   pkg load signal

   nb = 3 ;
   lrefl = 3 * nb ;  # filtfilt requirement

   N = size(x) ;
   Np = prod(N(2:end)) ;
   xx = reshape(x, N(1), Np) ;
   C = mat2cell(xx, N(1), ones(1, Np)) ;

   xs = parfun(@(lt) _lt (SF, lt, C), (1:N(1))', "UniformOutput", false) ;
   xs = cell2mat(xs) ;
   
   xs = reshape(xs, N) ;

endfunction


## usage: res = _lt (SF, lt, C)
##
##
function res = _lt (SF, lt, C)
   [bw.b bw.a] = butter(3, 1/SF(lt)) ;
##   ffun = @(x) gfiltfilt(bw.b, bw.a, x) ;
   ffun = @(x) filtfilt(bw.b, bw.a, x) ;
   res = parfun(@(c) _sfilter(ffun, c, lt), C, "UniformOutput", false) ;
   res = cell2mat(res) ;
endfunction


## usage: _xs = _sfilter (f, x, lt)
##
## seamless filter
function _xs = _sfilter (f, x, lt)

   I = ~isnan(x) ;

   if all(~I)
      _xs = NaN ;
      return
   endif

   xm = mean(x(I)) ;
   x(~I) = xm ;

   xf = feval(f, x) ;
   
   _xs = xf(lt,:) ;

endfunction
