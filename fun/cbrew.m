## usage: c = cbrew (cl, cu, q1, q2, n)
##
## my own color brewer
## colormap between cl and cu with white at q
function c = cbrew (cl, cu, q1, q2, n)

   w = [1 1 1] ;
   cl1 = cl / 2 ;
   cu1 = cu / 2 ;
   
   n4 = floor(n / 4) ;

   t = (1:n4)' / n4 ;
   c11 = (1 - t) .* cl1 + t .* cl ;
   c12 = (1 - t) .* cl + t .* w ;
   c21 = (1 - t) .* w + t .* cu ;
   c22 = (1 - t) .* cu + t .* cu1 ;

   c = [c11 ; c12 ; c21 ; c22] ;

   i1 = lookup((1:n)/n, q1) + 1 ;
   i2 = lookup((1:n)/n, q2) ;
   
   c = c(i1:i2,:) ;
   
endfunction
