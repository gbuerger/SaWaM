## usage: out = ngr (x, y, yq, theta)
##
## CV wrapper for ngr_
function out = ngr (x, y, yq, theta)

   addpath("~/SaWaM/fun", "~/xds/fun", "~/xds/util")
   addpath ~/oct/nc/MLToolbox/MeteoLab/Validation
   pkg load statistics signal optim communications

   global QFOLD NFOLD

   x = squeeze(x) ;
   y = squeeze(y)(:) ;

   II = all(~isnan([x y]), 2) & any(abs(x) > 1e-10, 2) ;
   x = x(II,:) ;
   y = y(II,:) ;

   x(abs(x) < 1e-10) = 0 ;
   y(abs(y) < 1e-10) = 0 ;

   N = size(x) ;

   ## full estimate
   I = 1 : N(1) ;
   out = ngr_ (x, y, I, I, yq, theta) ;

   out.par.mu = fillnan(out.par.mu, II) ;
   out.par.sgm = fillnan(out.par.sgm, II) ;
   out.F = fillnan(out.F, II) ;

   ## cross validation
   I = arrayfun(@(i) randperm(N(1), round(QFOLD*N(1)))(:), 1:NFOLD, "UniformOutput", false) ;
   In = cellfun(@(i) setdiff(1:N(1), i)', I, "UniformOutput", false) ;

   wout = cellfun(@(i,in) ngr_ (x, y, i, in, yq, theta), I, In, "UniformOutput", false) ;

   for k = {"theta" "rho" "rpss" "crpss" "rsa"}
      out.(k{:}) = cellmean({[wout{:}].(k{:})}) ;
   endfor
   
endfunction


## usage: out = ngr_ (x, y, I, In, yq, theta)
##
## Non-homogenous Gaussian regression, foll. Wilks (2006) and Gneiting et al. (2005)
function out = ngr_ (x, y, I, In, yq, theta)

   global DIST OPTS SCORES
   
   switch DIST

      case "crps"

	 cost = @(theta) crps_G(x, y, I, theta) ;
	 
      otherwise

	 cost = @(theta) ign(x, y, I, theta) ;
	 
   endswitch

   out = struct("theta", theta, "par", NaN, "F", NaN, "skill", NaN, "rho", NaN, "rpss", NaN, "crpss", NaN) ;

   graph = false ; rho = NaN ;
   if strcmp(DIST, "emp")
      [~, par.mu, par.sgm] = anom(x, [], [], 2) ;
      rho = corr(y, par.mu) ;
   else
      [theta, fval, flag, output] = fminsearch (cost, theta, OPTS) ;
      if flag ~= 1
	 error("fminsearch did not converge, flag = %d", flag) ;
      endif   
      par = theta2par(theta, x) ;
   endif

   crps = mean(stk_distrib_normal_crps(y(In), par.mu(In), par.sgm(In))) ;
   mu0 = mean(y) ; sgm0 = std(y) ; # use unique ref forecast for CV
   crps0 = mean(stk_distrib_normal_crps(y, mu0, sgm0)) ;
   crpss = 1 - crps / crps0 ;

   indI = @(u) find(nthargout(2, @sort, [u yq]) == 1) ;
   O = arrayfun(@(v) indI(v), y(In)) ;

   Iq = [yq Inf] ;
   F = cell2mat(arrayfun(@(mu, sgm) normcdf(Iq, mu, sgm), par.mu, par.sgm, "UniformOutput", false)) ;
   P(:,1,:) = diff([0*F(:,1) F], [], 2) ;

   addpath ~/oct/nc/MLToolbox/MeteoLab/common
   [rps, rpss] = validationRPS(O, P(In,:,:), 1:3) ;

   wrn = warning() ;
   warning("off", "Octave:divide-by-zero") ;
   skill(:,1) = validationBinaryProb([], y(In) < yq(1), P(In,1,1), SCORES, "points", 20, "graph", graph) ;
   skill(:,2) = validationBinaryProb([], y(In) > yq(2), P(In,1,end), SCORES, "points", 20, "graph", graph) ;
   warning(wrn) ;
   rmpath ~/oct/nc/MLToolbox/MeteoLab/common

   rsa = [skill.rsa] ;
   
   out = struct("theta", theta, "par", par, "F", F, "rho", rho, "rpss", rpss, "crpss", crpss, "rsa", rsa) ;
   
endfunction


## usage: [f z] = crps_G (x, y, I, theta)
##
## cost function for CRPS (Wilks 2006, Eq. 3.9; Gneiting et al. 2005)
function f = crps_G (x, y, I, theta)
   par = theta2par(theta, x(I,:)) ;
   z = (y(I) - par.mu) ./ par.sgm ;
   u = par.sgm .* (z .* (2*normcdf(z) - 1) + 2*normpdf(z) - 1/sqrt(pi)) ;
   f = mean (u(isfinite(u))) ;
endfunction


## usage: f = ign (x, y, I, theta)
##
## ignorance score for Gaussian (Gneiting et al. 2005)
function f = ign (x, y, I, theta)
   par = theta2par(theta, x(I,:)) ;
   u = -log(normpdf(y(I), par.mu, par.sgm)) ;
   f = mean (u(isfinite(u))) ;
endfunction
