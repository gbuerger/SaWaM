## usage: s = read_ERA5 (ncf, Y=[], J=[])
##
## read ERA5 data
function s = read_ERA5 (ncf, Y=[], J=[])

   printf("read_ERA5> <-- %s\n", ncf) ;

   pkg load netcdf
   
   t0 = datenum(1950, 1, 1) ;
   t = ncread(ncf, "time") ;
   s.id = datevec(t0 + t)(:,1:3) ;
   s.x = ncread(ncf, "tp")' ;

   if ~isempty(J) s.x = nanmean(s.x(:,J), 2) ; endif
   
   if ~isempty(Y)
      I = Y(1) <= s.id(:,1) & s.id(:,1) <= Y(2) ;
      s.id = s.id(I,:) ;
      s.x = s.x(I,:) ;
   endif
   
   s.vt = datenum(s.id) ;

endfunction
