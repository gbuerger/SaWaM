## Copyright (C) 2008 Gerd Buerger
## 
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

## moc

## Author: Gerd Buerger <b324017@ds8>
## Created: 2008-11-13

function [a, s, r] = moc (o, f)
  ##
  ## usage:  [a, s, r] = moc (o, f)
  ## return moc entries from binary series (o: obs; f: forc)
  [rf, cf] = size (f);
  [ro, co] = size (o);
  if (co == 1 & cf > 1)
    o = repmat (o, 1, cf);
  endif
  I = isfinite (f) & isfinite (o);
  n = sum (I);
  a = sum (I & f > 0 & o > 0) ./ n;
  s = sum (I & o > 0) ./ n;
  r = sum (I & f > 0) ./ n;
endfunction
