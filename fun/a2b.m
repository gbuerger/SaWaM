## usage: y = a2b (x, a, b)
##
## replace a with b
function y = a2b (x, a, b)
   y = x ;
   y(y == a) = b ;
endfunction
