function varargout = errfun (S, varargin)

   ## usage:  varargout = errfun (S, varargin)
   ##
   ## ErrorHandler

   warning(S.identifier, sprintf("(%d): %s\n", S.index, S.message)) ;

   for i = 1 : nargout
      varargout{i} = NaN ;
   endfor

endfunction
