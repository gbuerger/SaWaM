## usage: xs = sfilter (x, GF, LTx)
##
## apply seamless filter
function xs = sfilter (x, GF, LTx)

   pkg load signal
   
   N = size(x) ;
   Np = prod(N(2:end)) ;
   xx = reshape(x, N(1), Np) ;
   C = mat2cell(xx, N(1), ones(1, Np)) ;

   clear xs ;
   for lt = 1:LTx
      w = parfun(@(c) _sfilter(c, GF, lt), C, "UniformOutput", false) ;
      xs(lt,:) = cell2mat(w) ;
   endfor
   xs = reshape(xs, N) ;

endfunction


## usage: _xs = _sfilter (x, GF, lt)
##
## seamless filter
function _xs = _sfilter (x, GF, lt)

   [bw.b bw.a] = butter(3, 1/GF(lt)) ;
   x = x(1:lt) ;
   
   I = ~isnan(x) ;

   if all(~I)
      _xs = NaN ;
      return
   endif

   xm = mean(x(I)) ;
   x(~I) = xm ;

   xf = filter(bw.b, bw.a, x) ;

   _xs = xf(lt,:) ;

endfunction
