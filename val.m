
if exist("BATCH", "var") == 1 && ~BATCH
   clear -a ;
   BATCH = false ;
endif
cd ~/SaWaM ;

addpath("~/SaWaM/fun", "~/xds/fun", "~/xds/util") ;
addpath ~/oct/nc/MLToolbox/MeteoLab/Validation
addpath ~/oct/nc/MLToolbox/MeteoLab/common
pkg load statistics signal optim communications
HOME = getenv("HOME") ;
set(0, "defaultfigureunits", "normalized", "defaultfigurepaperunits", "normalized") ;
set(0, "defaultfigureposition", [0.7 0.7 0.3 0.3], "defaultfigurepaperpositionmode", "auto") ;
set(0, "defaultaxesgridalpha", 0.3)
set(0, "defaultlinelinewidth", 2)
set(0, "defaultaxesfontsize", 18, "defaulttextfontsize", 18)
set(0, "defaultaxesfontname", "Linux Biolinum", "defaulttextfontname", "Linux Biolinum") ;
vis = "off" ;

## parameters
ja = str2num(getenv("ja")) ;
jv = str2num(getenv("jv")) ;
jr = str2num(getenv("jr")) ;
if isempty(jv)
   [ja jv jr] = deal(1, 1, 1) ; # XDS or KIT and region
endif
if ja == 1 && jv == 2 exit(0) ; endif

REF = {"STN" "ERA5L" "STN"}{jv} ;
REF = {"STN" "STN" "STN"}{jv} ;
##REF = {"ERA5L" "ERA5L" "ERA5L"}{jv} ;
sim = Sim = {"SEAS5" "KIT" "SEAS5.bc"}{jv} ;
as = {"a" "ptr"}{ja} ;
if strcmp(as, "a") Sim = strrep(sim, "SEAS5", "XDS") ; endif
PDD = {[as "out"] "KIT" [as "out"]}{jv} ;
V = {"P" "SEAS5_BCSD_v2.1.P" "P"}{jv} ;
Tx = [181 180 181](jv) ;
reg = {"iran" "brazil" "SHIVA"}{jr} ;
RB = {"KB" "SFB" "Mahanadi"}{jr} ;
mfx = {"SaWaM" "SaWaM" "."}{jr} ;
pfile = {"irmo" "inmet" "pdd"}{jr} ;
nY = [38 38 37](jr) ;
iM = [1 12 4](jr) ;

printf("  REF = %s\n  sim = %s\n  PDD = %s\n  Basin = %s\n  pfile = %s\n", REF, sim, PDD, RB, pfile) ;

YLBL = "P  [mm]" ;
global PAR SIDED = "one" ;
PAR.waitbar = true ;
xroot = sprintf("../xds/data/%s/%s", mfx, reg) ;
droot = sprintf("%s/ptr/%s/%s/%s", xroot, mfx, reg, sim) ;
[dLT dNAT dMON dMET dHYD dSYS] = deal(1,2,3,4,5,6) ;
FAC = {"LT" "NAT" "MON" "MET" "HYD" "SYS"} ;
[nNAT nMON nMET nHYD nSYS] = deal(nY, 12, 25, 1, 1) ;
vars = {"MET" "MON" "HYD"} ;
Ax = [150 7 Tx](1) ;			# maxiumum smoothing of G filter
xi = [2.4 100 10 4](1) ;

## derivates
SF = @(x) Ax - (Ax-1) * ((Tx-x) ./ (Tx-1)).^xi ;
nv = length(vars) ;


## program
if 0
   figure(1) ; clf ;
   plot(SF(1:Tx), "linewidth", 3) ;
   grid on ; box off ;
##   xlim([0 Tx]) ; ylim([0 SF(190)]) ; box off
   xlabel("lead time  [d]") ; ylabel("cutoff period {\\itA}  [d]") ; 
   title("smoothing vs. lead time of seamless filter", "fontsize", 18) ;
   ht = text(10, 180, sprintf("\\it A(d) = A_x - (A_x-1) * (T_x-d) / (T_x-1)^\\xi")) ;
   ht2 = text(10, 140, sprintf("\\it T_x = %d; A_x = %d, \\xi = %.1f", Tx, Ax, xi)) ;
   print(sprintf("nc/filterchar.svg")) ;
endif

pmkdir(Xdir = sprintf("data/%s/%s/%s", reg, sim, PDD)) ; Xfile = sprintf("%s/X.ob", Xdir) ;
if isempty(glob(str = sprintf("%s/????", droot))) || isnewer(Xfile, glob(str){1})

   load(Xfile) ;

else

   warning ("off", "Octave:legacy-function") ;
   if PAR.waitbar, hw = waitbar(0, "looping...") ; endif
   ID = nan(Tx,nNAT,nMON,nMET,nHYD,nSYS,3) ;
   X = nan(Tx,nNAT,nMON,nMET,nHYD,nSYS) ;
   iloop = 0 ; nloop = nHYD * nMON ;
   for iHYD = 1:nHYD
      for iMON = 1:nMON
	 iNAT = 0 ; iloop++ ;
	 if isempty(F = glob(sprintf("%s/[0-9][0-9][0-9][0-9]", droot))')
	    error("No files in %s", sprintf("%s/[0-9][0-9][0-9][0-9]", droot)) ;
	 endif
	 for yr = F
	    if strcmp(yr{:}(end-3:end), "2019") break ; endif
	    iNAT++ ;
	    if jv == 2
	       ifile = sprintf("%s/%02d.%s.csv", yr{:}, iMON, V) ;
	    else
	       ifile = sprintf("%s/%s.%s/%02d.%s.csv", yr{:}, pfile, PDD, iMON, V) ;
	    endif
	    if exist(ifile, "file") ~= 2 continue ; endif
	    if ~isfigure(hw)
	       printf("%7.2f%%: <-- %s\n", 100*real(iloop/nloop), ifile) ;
	       fflush(stdout) ;
	    endif
	    [id x] = read_XDS(ifile, nMET) ;
	    nx = rows(x) ;
	    for iSYS=1:nSYS
	       ID(1:nx,iNAT,iMON,:,iHYD,iSYS,:) = id ;
	       X(1:nx,iNAT,iMON,:,iHYD,iSYS) = x ;
	    endfor
	 endfor
	 if PAR.waitbar
	    waitbar(iloop/nloop, hw, sprintf("%d --> %d", iloop, nloop)) ;
	 else
	    printf("%d --> %d", iloop, nloop) ;
	    fflush(stdout) ;
	 endif
      endfor
   endfor
   if PAR.waitbar, close(hw) ; endif ;
   pmkdir(sprintf("data/%s/%s/%s", reg, sim, PDD)) ;
   save(Xfile, "X", "ID") ;
##   if 0 & BATCH, exit ; endif
   
endif

if isnewer(sfile = sprintf("data/%s/%s/%s/Xs.Ax-%d-%.1f.ob", reg, sim, PDD, Ax, xi), Xfile)
   load(sfile) ;
else
   XS = sfilter(X, SF) ;
   save(sfile, "XS") ;
   if exist("BATCH", "var") == 1 && BATCH exit ; endif
endif


ID = squeeze(ID) ;
N = size(ID) ;

## plot mean Q/P
if exist(Tdfile = sprintf("data/%s/Td.ob", reg), "file") == 2
   load(Tdfile) ;
   vt = vt(1:N(1),:,:,:) ;
else
   CID = mat2cell(ID, ones(N(1),1), ones(N(2),1), ones(N(3),1), ones(N(4),1), N(5)) ;
   I = cellfun(@(c) all(~isnan(c)), CID) ;
   Nx = size(X) ;
   vt = Td = nan(Nx) ;
   vt(I) = parfun(@(c) datenum(num2cell(c){:}), CID(I)) ;
   Td(I) = parfun(@(u) doy(u), vt(I)) ;
   save(Tdfile, "Td", "vt") ;
endif

## prediction skill

##load OBS
pmkdir(adir = sprintf("data/%s/val", reg)) ;
afile = sprintf("%s/%s.ob", adir, pfile) ;
if exist(afile, "file") == 2

   load(afile) ;

else

   load(sprintf("%s/%s/%s.ob", xroot, pfile, pfile)) ;
   pdd.x = nanmean(pdd.x(:,pdd.isP), 2) ;

   ## compare IRMO and KWPA
   if strcmp(reg, "iran")
      load(sprintf("data/%s/%s.ob", reg, pfile)) ;
      irmo = pdd ;
      irmo.J = irmo.isP & irmo.lons ~= lim & irmo.lats ~= lim ;
      load("../xds/data/SaWaM/iran/pdd/pdd.ob") ;
      kwpa = pdd ;
      kwpa.J = kwpa.isP & kwpa.lons ~= lim & kwpa.lats ~= lim ;

      ptr = read_stat("boxUP/iran/ERA5.csv") ;
      ##      select Karkeh, Karun, Marun, Zoreh
      ana = read_ERA5("boxUP/iran/ERA5_Land_daily_1981_2019_TS_Khuzestan.nc", [], 1:4) ;
      [irmo.I kwpa.I ptr.I ana.I] = common(irmo.id, kwpa.id, ptr.id, ana.id) ;

      if 0
	 clf ; sv = 0.2 ;
	 ##      subaxis(2,1,1, "sv", sv) ;
	 subplot(2,1,1) ;
	 [d rho] = correlogram(irmo, irmo, irmo.J, irmo.J) ;
	 plot(d(:), rho(:), ".")
	 xlabel("distance [km]") ; ylabel("{\\rho}") ;
	 title(sprintf("correlogram IRMO"))
	 ##      subaxis(2,1,2, "sv", sv) ;
	 subplot(2,1,2) ;
	 [d rho] = correlogram(kwpa, kwpa, kwpa.J, kwpa.J) ;
	 plot(d(:), rho(:), ".")
	 xlabel("distance [km]") ; ylabel("{\\rho}") ;
	 title(sprintf("correlogram KWPA"))

	 print(sprintf("boxUP/%s/correlogram.png", reg)) ;
	 ##      [d rho] = correlogram(irmo, kwpa, irmo.J, kwpa.J) ;
	 ##      plot(d(:), rho(:), ".")

	 ##      plot(ptr.x(ptr.I), ana.x(ana.I), "x")
	 clf ;
	 h = plot(dateax(ptr.id(ptr.I,:)), [irmo.x(irmo.I) kwpa.x(kwpa.I) ana.x(ana.I)]) ;
	 xlim(10*365+[724301 724400]) ;
	 col = [0 0 0 ; 0 0 1 ; 0 1 0] ;
	 arrayfun(@(j) set(h(j), "Color", col(j,:)), 1:3)
	 datetick("dd-mmm", "keeplimits") ;
	 title(sprintf("stations vs ERA5\\_Land\n%s", RB))
	 box off
	 legend({"IRMO" "KWPA" "ERA5 Land"}, "box", "off")
	 r = nancorrcoef([irmo.x(irmo.I) kwpa.x(kwpa.I) ana.x(ana.I)])
	 disp(mean([irmo.x(irmo.I) kwpa.x(kwpa.I) ana.x(ana.I)])) ;
	 ht = text(get(gca, "xtick")(5), get(gca, "ytick")(5) - 5, sprintf("\\rho(IRMO/E5L) = %3.2f", r(1,3))) ;
	 ht = text(get(gca, "xtick")(5), get(gca, "ytick")(5) - 10, sprintf("\\rho(KWPA/E5L) = %3.2f", r(2,3))) ;
	 ht = text(get(gca, "xtick")(5), get(gca, "ytick")(5), sprintf("\\rho(IRMO/KWPA) = %3.2f", r(2,1))) ;
	 print(sprintf("boxUP/%s/ERA5L_cmp.png", reg)) ;
      endif
   else
      ana = read_ERA5("boxUP/brazil/ERA5_Land_daily_1981_2019_TS_SF_Basin.nc", [], 1) ;
   endif

   pdd.vt = datenum(pdd.id) ;
   pdd.Ivt = parfun(@(a) nanfind(pdd.vt == a), vt) ;
   I = ~isnan(pdd.Ivt) ;
   pdd.xx = nan(size(pdd.Ivt)) ;
   pdd.xx(I) = pdd.x(pdd.Ivt(I)) ;

   ana.vt = datenum(ana.id) ;
   ana.Ivt = parfun(@(a) nanfind(ana.vt == a), vt) ;
   I = ~isnan(ana.Ivt) ;
   ana.xx = nan(size(ana.Ivt)) ;
   ana.xx(I) = ana.x(ana.Ivt(I)) ;

   save(afile, "pdd", "ana") ;

endif

pmkdir(sdir = sprintf("data/%s/val", reg)) ;
if isnewer(sfile = sprintf("%s/Xs-%d-%.1f.ob", sdir, Ax, xi), afile)
   load(sfile) ;
else
   xs = sfilter(pdd.xx, SF) ;
   anas = sfilter(ana.xx, SF) ;
   save(sfile, "xs", "anas") ;

   ## plot STN-ERA5L correlation
   w1 = load("data/iran/val/Xs-150-2.4.ob") ;
   w2 = load("data/brazil/val/Xs-150-2.4.ob") ;
   clf ; hold on ;
   plot(parfun(@(i) nancorrcoef(w1.xs(i,:,:,1)(:), w1.anas(i,:,:,1)(:)), 1:180), "color", 0.7*[0 0 1]) ;
   plot(parfun(@(i) nancorrcoef(w2.xs(i,:,:,1)(:), w2.anas(i,:,:,1)(:)), 1:180), "color", 0.7*[0 1 0]) ;
   xlabel("lead time [d]") ; ylabel("{\\rho}") ;
   title(sprintf("STN-ERA5L correlation vs. lead time (SF smoothing)")) ;
   legend({"KB" "SFB"}, "box", "off", "location", "southeast") ;
   print(sprintf("boxUP/STN-ERA5L.svg")) ;
endif


### categorical forecasts
global DIST OPTS SCORES QFOLD NFOLD
SCORES = {"roc" "rsa"} ; QFOLD = 2/3 ; NFOLD = 5 ;
N = size(X) ; N(1) = 180 ;
q.q = [1/3 2/3] ;     # event quantile

## use seasonal quantiles
if exist(ofile = sprintf("data/%s/%s/%s/out.%s.ob", reg, sim, PDD, REF), "file") == 2

   printf("<-- %s\n", ofile) ;
   load(ofile) ;
   if strcmp(REF, "STN")
      Cy = squeeze(mat2cell(xs(1:N(1),:,:,1), ones(N(1),1), N(2), ones(N(3),1), 1)) ;
   else
      Cy = squeeze(mat2cell(anas(1:N(1),:,:,1), ones(N(1),1), N(2), ones(N(3),1), 1)) ;
   endif
   
else

   ## fix first dimension
   XS = XS(1:N(1),:,:,:) ;
   xs = xs(1:N(1),:,:,:) ;

   ## Non-homogenous Gaussian regression
   CXS = squeeze(mat2cell(XS, ones(N(1),1), N(2), ones(N(3),1), N(4))) ;
   if strcmp(REF, "STN")
      Cy = squeeze(mat2cell(xs(:,:,:,1), ones(N(1),1), N(2), ones(N(3),1), 1)) ;
   else
      Cy = squeeze(mat2cell(anas(:,:,:,1), ones(N(1),1), N(2), ones(N(3),1), 1)) ;
   endif
   Cq = cellfun(@(y) quantile(y, q.q), Cy, "UniformOutput", false) ;
   theta0 = [0 1 0 1] ;
   OPTS = optimset("TolX", 1e-3, "TolFun", 5e-1, "MaxIter", 2000, "MaxFunEvals", 2000, "Display", "notify") ;
   for DIST = {"emp" "ml" "crps"}
      DIST = DIST{:} ;
      out.(DIST) = parfun(@(x,y,yq) ngr(x, y, yq, theta0), CXS, Cy, Cq, "UniformOutput", false) ;
   endfor
   
   ## Logistic regression
   OPTS = optimset("TolX", 1e-3, "TolFun", 5e-1, "MaxIter", 2000, "MaxFunEvals", 2000, "Display", "notify") ;
   out.LR_nlf = parfun(@(x,y,yq) lreg(x, y, yq, "nlinfit"), CXS, Cy, Cq, "UniformOutput", false) ;
   out.LR_ml = parfun(@(x,y,yq) lreg(x, y, yq, "ml"), CXS, Cy, Cq, "UniformOutput", false) ;

   obs.id = pdd.id ;
   obs.x = log2(a2b(bi2de([pdd.x(:) < q.q(1) q.q(1) <= pdd.x(:) & pdd.x(:) < q.q(2) q.q(2) <= pdd.x(:)]), 0, NaN)) ;

   obs.names = {"P terciles"} ;
   obs.ids = {"areal mean"} ;
   obs.vars = {"tcl"} ;
   obs.lons = mean(pdd.lons) ;
   obs.lats = mean(pdd.lats) ;
   obs.alts = mean(pdd.alts) ;

   printf("--> %s\n", ofile) ;
   save(ofile, "out", "Cq", "Cy", "obs", "DIST") ;
   if exist("BATCH", "var") == 1 && BATCH exit ; endif

endif

## write netcdf
if 0
   odir = fullfile(mfx, reg, sim) ;
   ncout(pfile, odir, vt, out.ngr, mean([pdd.lons ; pdd.lats ; pdd.alts], 2)) ;
   odir = sprintf("%s/%s/%s", mfx, reg, pfile) ;
   ncwrite_stat(odir, obs, :, "~/SaWaM/Nextcloud") ;
endif

alpha = 0.1 ;
sfx = "svg" ;
jq = 1 ;
q.ln = {"lower tercile" "upper tercile"} ; q.sn = {"l" "u"} ; qfun = {@(x) x, @(x) 1-x} ; qcmp = {"<" ">"} ;
if 0
   source("scripts/plot_ts.m") ;
   source("scripts/plot_skl.m") ;
endif

## monthly skill averages
thm = 360/12 * (0:11) ;
J = linspace(1, 181, 7) ; J(end) = 180 ;
if jv == 2 J(end) -= 1 ; endif
for DIST = {"emp" "ml" "crps" "LR_nlf" "LR_ml"}
   DIST = DIST{:} ;
   if exist(sfile = sprintf("data/%s/%s/%s/skill.%s.%s.ob", reg, sim, PDD, DIST, REF), "file") ~= 2 || 1
      clear("crpss", "rho", "rpss", "rsa_l", "rsa_u") ;
      rpss = cellfun(@(c) c.rpss, out.(DIST)) ;
      rpss = cell2mat(arrayfun(@(j) mean(rpss(J(j-1):J(j),:))', 2:length(J), "UniformOutput", false)) ;
      SS = {"J", "rpss"} ;
      if isfield(out.(DIST){1,1}, "crpss")
	 crpss = cellfun(@(c) c.crpss, out.(DIST)) ;
	 crpss = cell2mat(arrayfun(@(j) mean(crpss(J(j-1):J(j),:))', 2:length(J), "UniformOutput", false)) ;
	 SS = union(SS, "crpss") ;
      endif
      if isfield(out.(DIST){1,1}, "rho")
	 rho = cellfun(@(c) c.rho, out.(DIST)) ;
	 rho = cell2mat(arrayfun(@(j) mean(rho(J(j-1):J(j),:))', 2:length(J), "UniformOutput", false)) ;
	 SS = union(SS, "rho") ;
      endif
      if isfield(out.(DIST){1,1}, "rsa")
	 for jq = 1:length(q.q)
	    w = cellfun(@(c) c.rsa(jq), out.(DIST)) ;
	    w = cell2mat(arrayfun(@(j) mean(w(J(j-1):J(j),:))', 2:length(J), "UniformOutput", false)) ;
	    eval(sprintf("rsa_%s = w ;", q.sn{jq})) ;
	 endfor
	 SS = union(SS, {"rsa_l" "rsa_u"}) ;
      endif
      printf("--> %s\n", sfile) ;
      save(sfile, SS{:}) ;
   endif
endfor
if exist("BATCH", "var") == 1 && BATCH exit ; endif


sfx = "svg" ;
sim = {"SEAS5" "SEAS5.bc" "SEAS5" "SEAS5.bc" "KIT"} ;
Sim = {"SEAS5" "SEAS5.bc" "XDS" "XDS.bc" "BCSD"} ;
Ref = {"STN" "STN" "STN" "STN" "ERA5L"} ;
PDD = {"ptrout" "ptrout" "aout" "aout" "KIT"} ;
V = {"P" "P" "P" "P" "SEAS5_BCSD_v2.1.P"} ;
reg = {"iran" "brazil" "SHIVA"} ;
RB = {"KB" "SFB" "Mahanadi"} ;
CX = [1.0 1.0 0.45 0.4] * 1.0 ;
JR = 1:2 ;
jbc = 1 ;
JV = {[1 3 5] [2 4 5]}{jbc} ; bc = {"" ".bc"}{jbc} ;

for js = 1 : 5
   skl = {"rsa_l" "rsa_u" "rpss" "crpss" "rho"}{js} ;
   for dist = {"emp" "crps" "LR_ml"}
      dist = dist{:} ;
      if exist(pf = sprintf("boxUP/%s.%s%s.%s", skl, dist, bc, sfx), "file") == 2 && 0 continue ; endif

      if strncmp(skl, "rsa", 3)
	 SKL = [toupper(skl(1:end-1)) skl(end)] ;
      else
	 SKL = toupper(skl) ;
      endif

      MON = arrayfun(@(m) {datestr(datenum([1 m 1]), "m")}, 1:12) ;
      figure(1, "position", [0.4 0.5 0.6 0.5]) ;
      sh = 0.03 ; sv = 0.1 ;
      clf ; clear ax h ;
      nv = length(JV) ; skill.(dist).(skl) = nskl = 0 ;
      for jr = JR
	 for jv = 1:length(JV)
	    of = sprintf("data/%s/%s/%s/skill.%s.%s.ob", reg{jr}, sim{JV(jv)}, PDD{JV(jv)}, dist, Ref{JV(jv)}) ;
	    clear(sprintf("%s", skl)) ;
	    load(of) ;
	    if exist(skl, "var") == 1 && ~all(isnan(eval(skl))(:))
	       nskl++ ;
	       eval(sprintf("skill.(dist).%s += %s ;", skl, skl)) ;
	       ax(jr,jv) = subaxis(2, nv+1, (jr-1)*(nv+1) + jv, "sh", sh, "sv", sv) ;
	       h(jr,jv) = eval(sprintf("bullseye(%s, 'N', length(J)-1, 'tht', [0 360], 'rho', [0 7], 'labels', MON) ;", skl)) ;
	    endif
	 endfor
      endfor
      skill.(dist).(skl) /= nskl ;
      if exist("ax", "var") ~= 1 continue ; endif
      ax(1,nv+1) = subaxis(2, nv+1, [nv+1 2*(nv+1)]) ;
      set(ax(1,1:nv), "xaxislocation", "top")
      set(ax(1:2,1:nv), "xlim", [-8 8], "ylim" , [-8 8], 'xtick', [], 'ytick', [], "box", "off");
      set(ax(1:2,1:nv), "xlim", [-8 8], "ylim" , [-8 8], 'color', 'white', 'xcolor', 'white', 'ycolor', 'white');
      ##XT = get(ax(1,1), "xticklabel") ; NT = length(XT) ;
      ##set(ax(1:2,1:2), "xticklabel", XT(ceil([NT:-1:NT/2 NT/2+1:NT])), "yticklabel", XT(ceil([NT:-1:NT/2 NT/2+1:NT]))) ;
      hx = arrayfun(@(jv) xlabel(ax(1,jv), sprintf("%s", Sim{JV(jv)}), "color", "black"), 1:nv) ;
      hy = arrayfun(@(jr) ylabel(ax(jr,1), sprintf("%s", RB{jr}), "color", "black"), 1:2) ;
      ##pos = get(hx, "position") ; pos(1) += 10 ; pos(2) -= 0 ; set(hx, "position", pos) ;
      ##pos = get(hy, "position") ; pos(2) += 10 ; pos(1) -= 0 ; set(hy, "position", pos) ;
      ##set(ax(1,1:2), "xaxislocation", "top") ; set(ax(1:2,2), "yaxislocation", "right") ;
      cn = eval(sprintf("min(%s(:)) ;", skl)) ;
      cx = eval(sprintf("max(%s(:)) ;", skl)) ;
      cn = 0 ;
      set([ax(1:2,1:nv)(:)' ax(1,nv+1)], "clim", [cn cx]) ;
      set(ax(1,nv+1), "visible", vis) ;
      hc = colorbar(ax(1,nv+1)) ; set(get(hc, "title"), "string", SKL, "fontangle", "italic") ;
      pos = get(hc, "position") ; pos(3) = 0.02 ; set(hc, "position", pos) ;
      ytl = get(hc, "yticklabel") ; ytl{1} = "≤0" ; set(hc, "yticklabel", ytl) ;
      ca = max(abs(cx), abs(cn)) ;
##      cn = min(cn, 0) ;
      if 0
	 if abs(cn) < abs(cx)
	    q = (cn + ca) / (2*ca) ;
	    cmap = cbrew([1 0 0], [0 1 0], q, 1, 100) ;
	 else
	    q = (cx + ca) / (2*ca) ;
	    cmap = cbrew([1 0 0], [0 1 0], 0, q, 100) ;
	 endif
	 colormap(cmap) ;
##	 i = lookind(0, cn, cx, 100) ;
##	 colormap(brewermap(100, "RdYlGn")(i:end,:)) ;
##	 colormap(redwhitegreen(100)(i:end,:)) ;
      else
##	 colormap(bluewhitered(100)) ;
	 colormap(flip(gray(100))) ;
      endif
      printf("--> %s\n", pf) ;
      print(pf) ;

   endfor
endfor
save("data/skill.ob", "skill") ;

exit(0) ;

load("data/skill.ob") ;
disp(mean(skill.emp.rpss(:))) ;
disp(mean(skill.LR_ml.rpss(:))) ;
set(1, "position", [0.55   0.67   0.45   0.33]) ;
colormap(flip(gray)) ;
js = 2 ;
skl = {"rsa_l" "rsa_u" "rpss" "crpss"}{js} ;
if strncmp(skl, "rsa", 3)
   SKL = [toupper(skl(1:end-1)) skl(end)] ;
else
   SKL = toupper(skl) ;
endif
clear ax ; clf ; nv = 4 ; sh = 0.05 ;
ax(1) = subaxis(1, nv+1, 1:2, "sh", sh) ;
bullseye(skill.emp.(skl), 'N', length(J)-1, 'tht', [0 360], 'rho', [0 7], 'labels', MON) ;
ax(2) = subaxis(1, nv+1, 3:4, "sh", sh) ;
bullseye(skill.crps.(skl), 'N', length(J)-1, 'tht', [0 360], 'rho', [0 7], 'labels', MON) ;
ax(nv+1) = subaxis(1, nv+1, 5) ;
set(ax(1:nv/2), "xaxislocation", "top")
set(ax(1:nv/2), "xlim", [-8 8], "ylim" , [-8 8], 'xtick', [], 'ytick', [], "box", "off");
set(ax(1:nv/2), "xlim", [-8 8], "ylim" , [-8 8], 'color', 'white', 'xcolor', 'white', 'ycolor', 'white');
##XT = get(ax(1,1), "xticklabel") ; NT = length(XT) ;
##set(ax(1:2,1:2), "xticklabel", XT(ceil([NT:-1:NT/2 NT/2+1:NT])), "yticklabel", XT(ceil([NT:-1:NT/2 NT/2+1:NT]))) ;
hx = arrayfun(@(jv) xlabel(ax(1,jv), sprintf("%s", {"RAW" "CAL"}{jv}), "color", "black"), 1:nv/2) ;
##pos = get(hx, "position") ; pos(1) += 10 ; pos(2) -= 0 ; set(hx, "position", pos) ;
##pos = get(hy, "position") ; pos(2) += 10 ; pos(1) -= 0 ; set(hy, "position", pos) ;
##set(ax(1,1:2), "xaxislocation", "top") ; set(ax(1:2,2), "yaxislocation", "right") ;
cn = eval(sprintf("min([skill.emp.%s(:) ; skill.crps.%s(:)]) ;", skl, skl)) ;
cx = eval(sprintf("max([skill.emp.%s(:) ; skill.crps.%s(:)]) ;", skl, skl)) ;
set(ax(1:nv/2), "clim", [cn cx]) ;
hc = colorbar(ax(nv+1)) ; set(get(hc, "title"), "string", SKL, "fontangle", "italic") ; set(ax(nv+1), "visible", vis) ;
pos = get(hc, "position") ; pos(3) = 0.02 ; set(hc, "position", pos) ;
pf = sprintf("boxUP/enscal.%s", sfx) ;
printf("--> %s\n", pf) ;
print(pf) ;
100 * (skill.crps.(skl) - skill.emp.(skl)) ./ abs(skill.emp.(skl))

jq = 1 ; jR = 2 ;
skl = "rpss" ; sskl1 = strsplit(skl, "_"){1} ; sskl2 = strsplit(skl, "_")(2:end) ;
sfx = "svg" ; sh = 0.1 ; sv = 0.1 ;
lloc = "northeast"
col = 0.7 * [0 0 0 ; 0 0 1 ; 0 1 0] ;
PRD = [{"SEAS5" "ptrout"} ; {"SEAS5" "aout"} ; {"KIT" "KIT"}] ; nPRD = rows(PRD) ;
REG = {"iran" "brazil"}{jR} ;
RB = {"KB" "SFB"} ;
MON = [11 12] ;

figure(1, "visible", vis, "position", [0.7 0.5 0.3 0.5]) ;
pf = sprintf("boxUP/%s/%s.%s", REG, skl, sfx) ;

clf ;
for iR = jR

   reg = REG ;

   for im = 1:length(MON)
      m = MON(im) ;
      ax(im) = subplot(2, 1, im) ;
      hold on ;
      for ir = 1 : nPRD
	 sim = PRD{ir,1} ; PDD = PRD{ir,2} ;
	 load(sprintf("data/%s/%s/%s/out.%s.ob", REG, sim, PDD, REF)) ;
	 Tx = rows(out.LR_ml) ;
	 if strcmp(sskl1, "rsa")
	    h(iR,ir) = plot(vt(1:Tx,1,m,1), arrayfun(@(j) out.LR_ml{j,m}.skill(jq).(sskl1), 1:Tx), "color", col(ir,:)) ;
	 else
	    h(iR,ir) = plot(vt(1:Tx,1,m,1), arrayfun(@(j) out.LR_ml{j,m}.(sskl1), 1:Tx), "color", col(ir,:)) ;
	 endif
      endfor
   endfor

endfor

##ax = gca ;
yl = [0 0.7] ;
set(ax, "ygrid", "on", "ylim", yl) ;
set(cell2mat(get(ax, "xlabel")), "string", "valid time") ;
set(cell2mat(get(ax, "ylabel")), "string", sprintf("%s%s", toupper(sskl1), sskl2{:}), "fontangle", "italic") ;
##set(ax(2), "yaxislocation", "right")
arrayfun(@(a) datetick(a, "m"), ax) ;
hl = arrayfun(@(a) legend(a, {"SEAS5" "XDS" "BCSD"}, "box", "off", "location", lloc), ax) ;
if m == 12
   pos = get(hl, "position") ; pos{1}(1) += 0.05 ; pos{2}(1) += 0.05 ; pos{1}(2) -= 0.06 ; pos{2}(2) -= 0.06 ;
   arrayfun(@(j) set(hl(j), "position", pos{j}), 1:2) ;
endif
TTL = arrayfun(@(m) datestr(vt(1,1,m,1), "mmm"), MON, "UniformOutput", false) ;
arrayfun(@(im) title(ax(im), sprintf("drought prediction %s\nissued: %s", RB{iR}, TTL{im})), 1:2) ;

set(gcf, "paperpositionmode", "auto")
printf("--> %s\n", pf) ;
print(pf) ;
